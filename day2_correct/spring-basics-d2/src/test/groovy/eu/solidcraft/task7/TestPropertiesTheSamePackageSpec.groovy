package eu.solidcraft.task7

import eu.solidcraft.IntegrationSpec
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.env.Environment
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.TestPropertySource
import spock.lang.Ignore

//TODO: Modify test configuration (add proper annotation) to make this test pass
@TestPropertySource("classpath:task7TestClasspathRoot.properties")
class TestPropertiesClasspathSpec extends IntegrationSpec {

    @Autowired
    private Environment environment

    def "test properties from root classpath should override production properties"() {
        expect:
            environment.getProperty("eu.solidcraft.testProperty") == "From root classpath"
    }
}

//TODO: Modify test configuration (add proper annotation) to make this test pass
//      Use different text representation to point classpath resource in the root project
@TestPropertySource("/task7TestClasspathRoot.properties")
class TestPropertiesClasspathVariant2Spec extends IntegrationSpec {

    @Autowired
    private Environment environment

    def "test properties from root classpath should override production properties"() {
        expect:
            environment.getProperty("eu.solidcraft.testProperty") == "From root classpath"
    }
}

//TODO: Modify test configuration (add proper annotation) to make this test pass
@TestPropertySource("/eu/solidcraft/task7/task7TestTheSamePackage.properties")
class TestPropertiesTheSamePackageSpec extends IntegrationSpec {

    @Autowired
    private Environment environment

    def "test properties from root classpath should override production properties"() {
        expect:
            environment.getProperty("eu.solidcraft.testProperty") == "From the same package"
    }
}
