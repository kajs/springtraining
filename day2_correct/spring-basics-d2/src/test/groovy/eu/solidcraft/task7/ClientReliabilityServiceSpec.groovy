package eu.solidcraft.task7

import spock.lang.Ignore
import spock.lang.Specification

class ClientReliabilityServiceSpec extends Specification {

    //Hint: Use Spock mocks - ClassToMock mock = Mock(ClassToMock)
    //      Stubbing - mock.method(arguments) >> result

    @Ignore
    def "should validate client when ok in BIK and KRD"() { //Unit test!

        //Hint: use noExceptionThrown() to show/verify that no exception was thrown
    }

    @Ignore
    def "should validate client when ok in BIK and not in KRD"() {
    }

    @Ignore
    def "should validate client when not ok in BIK and ok in KRD"() {
    }

    @Ignore
    def "should validate client when not ok in both BIK and KRD"() {
        //Hint: use thrown(Exception)
    }

    //TODO: Maybe the first 3 tests could be replaced with one parametrized test (with "where")?
}
