package eu.solidcraft.task7

import org.springframework.test.context.ContextConfiguration
import spock.lang.Ignore
import spock.lang.Specification

@ContextConfiguration(classes = ClientReliabilityConfiguration)
class ClientReliabilityServiceITSpec extends Specification {

    @Ignore
    def "should validate client when ok in BIK and KRD"() { //Integration test with Spring
    }

    //TODO: How to write other integration tests?
}
