package eu.solidcraft

import eu.solidcraft.task3.*
import groovy.transform.TypeChecked
import spock.lang.Ignore

@TypeChecked
class Task3Spec extends IntegrationSpec {

    @Ignore
    def "Register dogRepository and catRepository"() {
        expect:
            context.getBean(CatRepository) != null
            context.getBean(DogRepository) != null
    }

    @Ignore
    def "PetService should be registered twice, with different animals"() {
        expect:
            PetService dogService = context.getBean("dogService", PetService.class)
            dogService.getRandom() instanceof Dog
            PetService catService = context.getBean("catService", PetService.class)
            catService.getRandom() instanceof Cat
    }

    @Ignore
    def "Register catService under pussService"() {
        expect:
            context.getBean("catService") == context.getBean("pussService")
    }
}