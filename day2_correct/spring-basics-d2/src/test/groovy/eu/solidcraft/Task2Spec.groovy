package eu.solidcraft

import eu.solidcraft.task2.*
import spock.lang.Ignore

@Ignore
class Task2Spec extends IntegrationSpec {

    def "should have BookService in spring context"() {
        expect:
        context.getBean(AnnotatedBookService) != null
    }

    def "BookService should have repository injected"() {
        expect:
        context.getBean(AnnotatedBookService)?.bookRepository != null
    }


    def "Should have two BookRepositories registered in spring context"() {
        expect:
        context.getBeansOfType(AnnotatedBookRepository).size() == 2
    }


    def "BookRepository should have by injected by name"() {
        expect:
        context.getBean("inMemoryBookRepository") != null
        context.getBean("inFileBookRepository") != null
    }

    def "should have optional FileBookRepository injected via setter"() {
        expect:
        context.getBean(AnnotatedBookService)?.remoteBookRepository != null
    }

    //We could do it better with @DirtiesContext perhaps?
    def "should accept two repositories in constructor, or just one if only one is declared"() {
        expect:
        new AnnotatedTwoConstructorBookService(new AnnotatedInMemoryBookRepository())
        new AnnotatedTwoConstructorBookService(new AnnotatedInMemoryBookRepository(), new AnnotatedInFileBookRepository())
        context.getBean(AnnotatedTwoConstructorBookService).numberOfRepositoriesPresent() == 2
    }
}
