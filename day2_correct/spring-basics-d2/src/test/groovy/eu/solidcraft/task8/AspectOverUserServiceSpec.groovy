package eu.solidcraft.task8

import eu.solidcraft.IntegrationSpec
import org.springframework.beans.factory.annotation.Autowired
import spock.lang.Ignore

class AspectOverUserServiceSpec extends IntegrationSpec {

    @Autowired
    private UserService userService

    def "password should be hashed"() {
        expect:
            userService.getLoggedUserDetails().password == "#"
    }
}
