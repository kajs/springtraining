package eu.solidcraft

import eu.solidcraft.task4.InMemoryUserRepository
import eu.solidcraft.task4.OracleUserRepository
import eu.solidcraft.task4.StubUserRepository
import eu.solidcraft.task4.UserService
import org.springframework.test.context.ActiveProfiles
import spock.lang.Ignore

//See: @Profile

@Ignore
@ActiveProfiles("test")
class ProfileTestSpec extends IntegrationSpec {
    def "register a user service, with Stub user repository, in profile test"() {
        expect:
            context.getBean(UserService.class)?.userRepository instanceof StubUserRepository

    }
}

@Ignore
@ActiveProfiles("dev")
class ProfileDevSpec extends IntegrationSpec {
    def "register a user service, with InMemory user repository, in profile dev"() {
        expect:
            context.getBean(UserService.class)?.userRepository instanceof InMemoryUserRepository

    }
}

@Ignore
@ActiveProfiles("prod")
class ProfileProdSpec extends IntegrationSpec {
    def "register a user service, with Oracle user repository, in profile prod"() {
        expect:
            context.getBean(UserService.class)?.userRepository instanceof OracleUserRepository

    }
}

@Ignore
class ProfileDefaultSpec extends IntegrationSpec {
    def "register a user service, with Oracle user repository, in profile prod"() {
        expect:
            context.getBean(UserService.class)?.userRepository instanceof InMemoryUserRepository

    }
}
