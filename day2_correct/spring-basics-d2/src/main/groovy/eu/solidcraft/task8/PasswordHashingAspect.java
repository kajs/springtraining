package eu.solidcraft.task8;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class PasswordHashingAspect {

    @Around("eu.solidcraft.task8.PasswordHashingAspect.userDetailsOperation()")
    public Object hashingPassword(ProceedingJoinPoint pjp) throws Throwable {

        UserDetails userDetails = (UserDetails) pjp.proceed();
        userDetails.password = "#";
        return  userDetails;
    }

    @Pointcut("execution(UserDetails eu.solidcraft.task8.UserService.getLoggedUserDetails())")
    public void userDetailsOperation(){}
}
