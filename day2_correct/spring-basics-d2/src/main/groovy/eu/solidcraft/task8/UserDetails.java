package eu.solidcraft.task8;

public class UserDetails {
    public String password;
    public String login;

    public UserDetails(User user) {
        this.login = user.getLogin();
        this.password = user.getPassword();
    }
}
