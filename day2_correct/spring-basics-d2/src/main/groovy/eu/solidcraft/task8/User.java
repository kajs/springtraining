package eu.solidcraft.task8;

public class User {
    private String login = "Janek";
    private String password = "This should not be visible in logs and anywhere";

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }
}
