package eu.solidcraft.task8;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Configuration
@EnableAspectJAutoProxy
public class AspectsConfiguration {

    @Bean
    public UserService userService() {
        return new UserService();
    }

    @Bean
    public PasswordHashingAspect passwordHashingAspect() {
        return new PasswordHashingAspect();
    }
}
