package eu.solidcraft.task2;

public class AnnotatedTwoConstructorBookService {

    private AnnotatedInMemoryBookRepository inMemoryRepository;
    private AnnotatedInFileBookRepository inFileRepository;


    public AnnotatedTwoConstructorBookService(AnnotatedInMemoryBookRepository inMemoryRepository) {
        this.inMemoryRepository = inMemoryRepository;
    }

    public AnnotatedTwoConstructorBookService(AnnotatedInMemoryBookRepository inMemoryRepository, AnnotatedInFileBookRepository inFileRepository) {
        this.inMemoryRepository = inMemoryRepository;
        this.inFileRepository = inFileRepository;
    }

    public int numberOfRepositoriesPresent() {
        int numberOfRepos = 0;
        numberOfRepos += inMemoryRepository == null ? 0 : 1;
        numberOfRepos += inFileRepository == null ? 0 : 1;
        return numberOfRepos;
    }

}
