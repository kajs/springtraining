package eu.solidcraft.task2.config;

import eu.solidcraft.task2.AnnotatedBookRepository;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackageClasses = AnnotatedBookRepository.class)
//this class is in subdir, so you need to point to the root package class
public class Task2Configuration {
}
