package eu.solidcraft;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringExercisesApplication {

    public static void main(String[] args) {
        //TODO 1: override banner, using SpringApplicationBuilder, add your own
        //TODO 2: add two source classes: SpringExercisesApplication and  SpringExercisesMainConfiguration
        //TODO 2: add a listener that prints the list of source classes on application start
        //TODO 3: add a listener and print how long (number of seconds) the app was alive on closing. Does it work from IDE? Does it work wit ctrl+c? Why?
        SpringApplication.run(SpringExercisesApplication.class, args);
    }
}
