package eu.solidcraft.task7;

public class NotReliableClientException extends RuntimeException {

    private final String pesel;

    public NotReliableClientException(String pesel) {
        this.pesel = pesel;
    }

    public NotReliableClientException(String message, String pesel) {
        super(message);
        this.pesel = pesel;
    }

    public String getPesel() {
        return pesel;
    }
}
