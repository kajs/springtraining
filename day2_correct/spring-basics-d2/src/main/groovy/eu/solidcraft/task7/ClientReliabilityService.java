package eu.solidcraft.task7;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

public class ClientReliabilityService {

    private final BikChecker bikChecker;
    private final KrdChecker krdChecker;

    public ClientReliabilityService(BikChecker bikChecker, KrdChecker krdChecker) {
        this.bikChecker = bikChecker;
        this.krdChecker = krdChecker;
    }

    public void validateClientWithPesel(String pesel) {
        if (!(bikChecker.isClientOk(pesel) || krdChecker.isClientOk(pesel))) {
            throw new NotReliableClientException(pesel);
        }
    }
}
