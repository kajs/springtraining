package eu.solidcraft.task3;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

public class DogRepository implements PetRepository<Dog> {
    @Override
    public List<Dog> findAll() {
        return newArrayList(new Dog(), new Dog(), new Dog());
    }
}
