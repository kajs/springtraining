package eu.solidcraft.task3;

public class PetService {
    private PetRepository<? extends Pet> petRepository;

    public Pet getRandom() {
        return petRepository.findAll().get(0);
    }

    public PetService(PetRepository<? extends Pet> petRepository) {
        this.petRepository = petRepository;
    }
}
