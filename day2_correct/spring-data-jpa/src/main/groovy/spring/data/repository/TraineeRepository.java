package spring.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import spring.data.domain.Trainee;
import spring.data.domain.TraineeSummary;

import java.util.Collection;
import java.util.List;

@Repository
public interface TraineeRepository extends JpaSpecificationExecutor<Trainee>, JpaRepository<Trainee, Long> {

    List<Trainee> findByLastName(String lastName);
    List<Trainee> findByFirstNameIgnoreCase(String firstName);
    List<Trainee> findByFirstNameAndLastName(String firstName, String lastName);
    List<Trainee> findByFirstNameIn(Collection<String> firstName);
    List<Trainee> findByAddress_City(String city);
    Trainee findOneByFirstName(String firstName);

    //@Query(value="select new TraineeSummary(t.firstName, t.address.city) from Trainee t where t.address.city = :city")
    //List<TraineeSummary> findSummariesByAddressCity(@Param("city") String city);

    @Query(value="SELECT * FROM Trainee WHERE first_name = ?1 AND last_name = ?2", nativeQuery = true)
    List<Trainee> findByFirstNameAndLastNameNative(String firstName, String lastName);

    List<Trainee> findByCompanyLike(String companyPart);
}
