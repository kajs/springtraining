package spring.data.service;

import spring.data.domain.Trainee;

import java.util.List;

public interface TraineeService {

    Trainee findById(Long id);

    List<Trainee> findAll();

    long countAll();

    Trainee save(Trainee employee);

    Trainee update(Trainee employee);

    void delete(Trainee trainee);

    List<Trainee> findByLastName(String lastName);
}
