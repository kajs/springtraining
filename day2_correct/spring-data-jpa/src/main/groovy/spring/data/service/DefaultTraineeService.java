package spring.data.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import spring.data.domain.Trainee;
import spring.data.repository.TraineeRepository;

import java.util.List;

@Service
@Transactional
public class DefaultTraineeService implements TraineeService {

    private final TraineeRepository traineeRepository;

    @Autowired
    public DefaultTraineeService(TraineeRepository traineeRepository) {
        this.traineeRepository = traineeRepository;
    }

    @Override
    public Trainee findById(Long id) {
        return traineeRepository.findOne(id);
    }

    @Override
    public List<Trainee> findAll() {
        return traineeRepository.findAll();
    }

    @Override
    public long countAll() {
        return traineeRepository.count();
    }

    @Override
    public Trainee save(Trainee employee) {
        return traineeRepository.save(employee);
    }

    @Override
    public Trainee update(Trainee employee) {
        return traineeRepository.save(employee);
    }

    @Override
    public void delete(Trainee trainee) {
        traineeRepository.delete(trainee);
    }

    @Override
    public List<Trainee> findByLastName(String lastName) {
        return traineeRepository.findByLastName(lastName);
    }
}
