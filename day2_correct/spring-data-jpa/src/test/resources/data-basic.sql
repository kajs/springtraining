insert into Address (id, city, address_line, zip_code) values (200, 'Torun', 'Zielona 4', '04-123');
insert into Address (id, city, address_line, zip_code) values (201, 'Poznan', 'Towarowa 26', '61-896');
insert into Address (id, city, address_line, zip_code) values (202, 'Poznan', 'Fiołkowa 2/43', '61-129');

insert into Trainee (id, first_name, last_name, company, address_id) values (100, 'Henryk', 'Test', 'Unknown', 202);
insert into Trainee (id, first_name, last_name, company, address_id) values (101, 'Anita', 'Test2', 'Top Store', 200);
insert into Trainee (id, first_name, last_name, company, address_id) values (102, 'Henryk', 'Test3', 'Top Secret', 201);
