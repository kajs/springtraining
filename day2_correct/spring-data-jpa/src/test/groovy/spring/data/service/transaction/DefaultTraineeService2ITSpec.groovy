package spring.data.service.transaction

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.SpringApplicationContextLoader
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.TestPropertySource
import org.springframework.transaction.annotation.Transactional
import spock.lang.Ignore
import spock.lang.Specification
import spock.lang.Stepwise
import spring.data.SpringDataApplication
import spring.data.domain.Trainee
import spring.data.service.TraineeService

import static spring.data.domain.TraineeBuilder.aTestTrainee

@ContextConfiguration(classes = SpringDataApplication.class, loader = SpringApplicationContextLoader.class)
@TestPropertySource(properties = ["spring.datasource.platform=transaction"])
@Stepwise
@Transactional
class DefaultTraineeService2ITSpec extends Specification {

    @Autowired
    private TraineeService traineeService

    def "should save named trainee"() {
        given:
            Trainee trainee = aTestTrainee().build()
        when:
            traineeService.save(trainee)
        then:
            trainee.getId() != null
    }

    def "should throw exception on saving trainee without first name"() {
        given:
            Trainee trainee = aTestTrainee()
                    .withFirstName(null)
                    .build()
        when:
            traineeService.save(trainee)
        then:
            def e = thrown(DataIntegrityViolationException)
            e.message?.contains("ConstraintViolationException: could not execute statement")
    }

    def "should read loaded trainees"() {
        when:
            long numberOfTrainees = traineeService.countAll()
        then:
            numberOfTrainees == 2L
    }
}
