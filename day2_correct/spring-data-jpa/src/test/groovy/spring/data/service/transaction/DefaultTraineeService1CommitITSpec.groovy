package spring.data.service.transaction

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.SpringApplicationContextLoader
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.annotation.Rollback
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.TestPropertySource
import org.springframework.transaction.annotation.Transactional
import spock.lang.Ignore
import spock.lang.Specification
import spring.data.SpringDataApplication
import spring.data.domain.Trainee
import spring.data.service.TraineeService

import static spring.data.domain.TraineeBuilder.aTestTrainee

@ContextConfiguration(classes = SpringDataApplication.class, loader = SpringApplicationContextLoader.class)
@TestPropertySource(properties = ["spring.datasource.platform=transaction"])
@Transactional
class DefaultTraineeService1CommitITSpec extends Specification {

    @Autowired
    private TraineeService traineeService

    @DirtiesContext
    @Rollback(false)    //required to test commit based behaviors - like optimistic locking
    def "should save and commit trainee"() {
        given:
            Trainee trainee = aTestTrainee().build()
        when:
            traineeService.save(trainee)
        then:
            trainee.getId() != null
    }
}
