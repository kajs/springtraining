package spring.data.service.transaction

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.SpringApplicationContextLoader
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.TestPropertySource
import org.springframework.transaction.annotation.Transactional
import spock.lang.Ignore
import spock.lang.Specification
import spring.data.SpringDataApplication
import spring.data.domain.Trainee
import spring.data.domain.TraineeBuilder
import spring.data.repository.TraineeRepository
import spring.data.service.TraineeService

@ContextConfiguration(classes = SpringDataApplication.class, loader = SpringApplicationContextLoader.class)
@TestPropertySource(properties = ["spring.datasource.platform=transaction"])
class DefaultTraineeServiceITSpec extends Specification {

    @Autowired
    private TraineeService traineeService

    def "should read loaded trainees"() {
        when:
        List<Trainee> traineeList = traineeService.findAll();
        then:
        traineeList != null;
    }

    def "should save named trainee"() {
        given:
        //List<Trainee> traineeListBeforeAdd = traineeService.findAll();
        TraineeBuilder traineeBuilder = new TraineeBuilder();
        traineeBuilder = traineeBuilder.withFirstName("firstName")
                .withCompany("company2")
                .withLastName("lastName");
        Trainee trainee = traineeBuilder.build();
        when:
        traineeService.save(trainee);
        then:
        old(traineeService.countAll()) < traineeService.countAll();
        //old odczyta stara wartosc tak jakbysmy przypisali ja w given
        //List<Trainee> traineesFromDbAfterAdd = traineeService.findAll();
        //traineeListBeforeAdd.size() < traineesFromDbAfterAdd.size();
    }

    def "should throw exception on saving trainee without first name"() {
        given:
        TraineeBuilder traineeBuilder = new TraineeBuilder();
        traineeBuilder = traineeBuilder.withLastName("lastName")
                .withCompany("company");
        Trainee trainee = traineeBuilder.build();
        when:
        traineeService.save(trainee);
        then:
        thrown DataIntegrityViolationException;
    }
}
