package spring.data.domain;

public class TraineeBuilder {
    private long id;
    private String firstName;
    private String lastName;
    private String company;

    private TraineeBuilder() {
    }

    public static TraineeBuilder aTrainee() {
        return new TraineeBuilder();
    }

    public static TraineeBuilder aTestTrainee() {
        return aTrainee()
                .withFirstName("Foo")
                .withLastName("Testowy")
                .withCompany("Fajna");
    }

    public TraineeBuilder withId(long id) {
        this.id = id;
        return this;
    }

    public TraineeBuilder withFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public TraineeBuilder withLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public TraineeBuilder withCompany(String company) {
        this.company = company;
        return this;
    }

    public Trainee build() {
        Trainee trainee = new Trainee();
        trainee.setId(id);
        trainee.setFirstName(firstName);
        trainee.setLastName(lastName);
        trainee.setCompany(company);
        return trainee;
    }
}
