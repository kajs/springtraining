package spring.data

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.SpringApplicationContextLoader
import org.springframework.stereotype.Component
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.TestPropertySource
import org.springframework.transaction.annotation.Transactional
import spock.lang.Ignore
import spock.lang.Specification
import spring.data.domain.Trainee
import spring.data.domain.TraineeSummary
import org.springframework.data.jpa.domain.Specification as DataSpecification
import spring.data.repository.TraineeRepository

@ContextConfiguration(classes = SpringDataApplication.class, loader = SpringApplicationContextLoader.class)
@TestPropertySource(properties = ["spring.datasource.platform=basic"])
@Transactional(readOnly = true)
class SpringDataBaseQueriesSpec extends Specification {

    @Autowired
    private TraineeRepository traineeRepository;

    def "should get number of trainees"() {
        when:
            long numberOfTrainee = traineeRepository.count(); //TODO: Implement query using TraineeRepository
        then:
            numberOfTrainee == 3L
    }


    def "should read trainee by id"() {
        when:
            Trainee trainee = traineeRepository.findOne(101L)  //TODO: Implement query
        then:
            trainee.lastName == "Test2"
    }


    def "should read trainees with given ids"() {
        when:
            List<Trainee> trainees = traineeRepository.findAll([101L, 102L])     //TODO: Implement query - trainees with ID 101 and 102
        then:
            trainees.size() == 2
            trainees.firstName as Set == ['Anita', 'Henryk'] as Set
    }


    def "should find all trainees with given fist name"() {
        when:
            //Hint: Maybe that method could be just added to TraineeRepository? How to "implement" it?
            List<Trainee> henryks = traineeRepository.findByFirstNameIgnoreCase("Henryk")    //TODO: all people with first name Henryk
        then:
            henryks.size() == 2
    }


    def "should find all trainees with given fist name ignoring case"() {
        when:
            List<Trainee> henryks = traineeRepository.findByFirstNameIgnoreCase("henryk")    //TODO: all people with first name Henryk, ignoring case
        then:
            henryks.size() == 2
    }


    def "should find trainees by first and last name"() {
        when:
            List<Trainee> trainees = traineeRepository.findByFirstNameAndLastName("Henryk", "Test3")    //TODO: find trainee named: Henryk Test3
        then:
            trainees.size() == 1
            trainees[0].firstName == "Henryk"
            trainees[0].lastName == "Test3"
    }


    def "should find trainees with given names"() {
        when:
            List<Trainee> henryksAndAnitas = traineeRepository.findByFirstNameIn(["Henryk", "Anita"])   //TODO: All Henryks and Anitas
        then:
            henryksAndAnitas.size() == 3
    }


    def "should find trainees from Poznan"() {
        when:
            //Hint: It is possible to reach values in nested components
            List<Trainee> trainees = traineeRepository.findByAddress_City("Poznan")   //TODO: all people from Poznań
        then:
            trainees.size() == 2
    }


    def "should find trainee summaries from given city (JPA QL query)"() { //Challending
        when:
            List<TraineeSummary> summaries = traineeRepository.findSummariesByAddressCity("Torun")   //TODO: summaries of all people from Toruń - JPA QL query
        then:
            summaries.size() == 1
            summaries[0].name == 'Anita'
            summaries[0].city == 'Toruń'
    }


    def "should find trainee by first and last name (native query)"() { //Challending
        when:
            List<Trainee> trainees = traineeRepository.findByFirstNameAndLastNameNative("Henryk", "Test3")   //TODO: all people named "Henryk Test3" - native query
        then:
            trainees.size() == 1
            trainees[0].firstName == "Henryk"
            trainees[0].lastName == "Test3"
    }


    def "should find trainees from company begining with 'Top'"() { //Challending
        when:
            List<Trainee> trainees = traineeRepository.findByCompanyLike("Top%")
        then:
            trainees.size() == 2
    }

    def "should find trainees from company begining with 'Top' Data Spec"() { //Challending
        when:
        List<Trainee> trainees = null   //TODO: Use DataSpecification - alias for org.springframework.data.jpa.domain.Specification class
        then:
        trainees.size() == 2
    }


    def "should find trainees from company begining with 'Top' (Groovy Closure)"() {    //Challending
        when:
            List<Trainee> trainees = null   //TODO: Use DataSpecification - alias for org.springframework.data.jpa.domain.Specification class
        then:
            trainees.size() == 2
    }


    def "should find trainees from company starting with Top and with first name having length 5"() {   //Challending
        when:
            List<Trainee> trainees = null   //TODO: Write new Specification and combine it with existing one using
                                            //      helper methods from Specifications (plural)
        then:
            trainees.size() == 1
            trainees[0].firstName == "Anita"
    }
}
