### Spring Data

1. Uruchom z IDE SpringDataApplication. Na http://localhost:8080/beans znajdź obiekty bazodanowe. 

2. Patrząc w logi sprawdź, z jaką bazą działa aplikacja. W którym miejscu jest ona konfigurowana?

3. Zmień używaną bazę danych z HSQLDB na H2

4. Napisz kod naprawiający testy z klasy SpringDataBaseQueriesSpec.

5. Zaimplementuj testy w DefaultTraineeServiceITSpec.

6.1. Odignoruj testy w DefaultTraineeService2ITSpec
6.2. Dlaczego nie przechodzą?
6.3. Co można poprawić (w konfiguracji), aby zacząły działać?
  
7.1. Odignoruj testy w DefaultTraineeService1CommitITSpec 
7.2. Uruchom testy z tej klasy pojedynczo w IDE
7.3. Uruchom testy z DefaultTraineeService1CommitITSpec i DefaultTraineeService2ITSpec razem w IDE (lub z Gradle)
7.4. Dlaczego testy przestały przechodzić?
7.5. Co trzeba zrobić (w konfiguracji), aby testy te mogły być odpalane razem?
