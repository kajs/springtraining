# Spring basics - ćwiczenia - cześć 1

## Wprowadzenie

Projekt zawiera krótkie ćwiczenia pomagające zapoznać się z możliwościami platformy Spring Boot.


## Wymagania

Projekt wymaga zainstalowanego lokalnie [Java SDK](http://www.oracle.com/technetwork/java/javase/downloads/) w **wersji 8**.


## Budowa projektu

Projekt jest budowany z wykorzystaniem narzędzia [Gradle](http://www.gradle.org/), jednak dzięki zastosowaniu wrappera
nie jest konieczna jego wcześniejsza instalacja.

### Linia poleceń

Proste zbudowanie i wykonanie testów:

Linux/Unix:

    ./gradlew check

Windows:

    gradlew.bat check


### Import do IDE

#### IntelliJ Idea

Import do IntelliJ Idea: `File -> Import Project.`


#### Eclipse

Import do Eclipse:

    ./gradlew eclipse

i potem `File -> Import -> Existing Project Into Workspace`

**Uwaga**. Ze względu na wykorzystanie Java 8 zalecaną wersją Eclipsa jest minimum [Luna (4.4)](https://www.eclipse.org/downloads/).


## O projekcie

Autorzy:
 - [Jakub Nabrdalik](http://solidcraft.eu)
 - [Marcin Zajączkowski](http://www.solidsoft.pl)
