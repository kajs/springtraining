package eu.solidcraft

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.ConfigFileApplicationContextInitializer
import org.springframework.boot.test.SpringApplicationContextLoader
import org.springframework.context.support.AbstractApplicationContext
import org.springframework.test.context.ContextConfiguration
import spock.lang.Specification

//@SpringApplicationConfiguration(classes = SpringExercisesApplication) //or: @ContextConfiguration(classes = SpringExercisesApplication, initializers = ConfigFileApplicationContextInitializer)
@ContextConfiguration(classes = SpringExercisesApplication, initializers = ConfigFileApplicationContextInitializer, loader = SpringApplicationContextLoader)
class IntegrationSpec extends Specification {

    @Autowired
    protected AbstractApplicationContext context
}
