package eu.solidcraft

import eu.solidcraft.task5.ConnectionSettings
import groovy.transform.TypeChecked
import org.springframework.beans.factory.NoSuchBeanDefinitionException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ActiveProfiles
import spock.lang.Ignore

@TypeChecked
class Task5PropertiesSpec extends IntegrationSpec {

    private static final String EXPECTED_PROPERTY_VALUE = "Can you get me?"

    @Autowired
    private String propertyByValue
    @Autowired
    private String propertyFromEnvironment
    @Autowired
    private ConnectionSettings connectionSettings

    def "should be able to load properties"() {
        expect:
            propertyByValue == EXPECTED_PROPERTY_VALUE
            propertyFromEnvironment == EXPECTED_PROPERTY_VALUE
    }

    def "optional properties in test profile should be empty"() {
        when:
            context.getBean("optionalProperty")
        then:
            thrown(NoSuchBeanDefinitionException)
    }

    def "should populate ConnectionSettings from file"() {
        expect:
            connectionSettings.getUsername() == "admin"
            connectionSettings.getRemoteAddress().getHostAddress() == "192.168.1.1"
            //connectionSettings.getPorts().containsAll([80, 8080])
    }
}

@TypeChecked
@ActiveProfiles("dev")
class Task5OptionalPropertiesSpec extends IntegrationSpec {

    private static final String EXPECTED_OPTIONAL_PROPERTY_VALUE = "This is optional property that should be only in test profile"

    @Autowired
    private String optionalProperty

    def "should have optional properties"() {
        expect:
            optionalProperty == EXPECTED_OPTIONAL_PROPERTY_VALUE
    }
}
