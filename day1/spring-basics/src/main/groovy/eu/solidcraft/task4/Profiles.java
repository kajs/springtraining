package eu.solidcraft.task4;

public class Profiles {
    public static final String TEST = "TEST";
    public static final String DEV = "DEV";
    public static final String PROD = "prod";
    public static final String DEFAULT = "default";
}
