package eu.solidcraft.task4;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
public class ProfilesConfiguration {

    @Bean
    @Profile({Profiles.DEV, Profiles.DEFAULT})
    public UserService userServiceDev(){
        return new UserService(new InMemoryUserRepository());
    }


    @Bean
    @Profile(Profiles.PROD)
    public UserService userServiceProd(){
        return new UserService(new OracleUserRepository());
    }

    @Bean
    @Profile(Profiles.TEST)
    public UserService userServiceTest(){
        return new UserService(new StubUserRepository());
    }
}
