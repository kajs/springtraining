package eu.solidcraft;

import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextClosedEvent;

import java.io.PrintStream;
import java.sql.Time;
import java.util.Date;

@SpringBootApplication
public class SpringExercisesApplication {

    public static void main(String[] args) {
        //TODO 1: override banner, using SpringApplicationBuilder, add your own
        //TODO 2: add two source classes: SpringExercisesApplication and  SpringExercisesMainConfiguration
        //TODO 2: add a listener that prints the list of source classes on application start
        //TODO 3: add a listener and print how long (number of seconds) the app was alive on closing. Does it work from IDE? Does it work wit ctrl+c? Why?
        //SpringApplication.run(SpringExercisesApplication.class, args);
        SpringApplicationBuilder springApplicationBuilder = new SpringApplicationBuilder();
        Banner banner = (environment, sourceClass, out) -> System.out.println("test");
        ApplicationListener listener = new ApplicationListener<ApplicationStartedEvent>() {
            @Override
            public void onApplicationEvent(ApplicationStartedEvent event) {
                System.out.println("sourcy: "+ event.getSource());
                Long now = new Date().getTime();
                System.out.println("start: " + now);
            }
        };
        ApplicationListener closeListener = new ApplicationListener<ContextClosedEvent>() {
            @Override
            public void onApplicationEvent(ContextClosedEvent event) {
                Long now = new Date().getTime();
                Long runtime = now - event.getApplicationContext().getStartupDate();
                System.out.println("koniec: " + now);
                System.out.println("dzialalo: " + runtime);
            }
        };
        springApplicationBuilder.showBanner(true)
                .sources(SpringExercisesApplication.class, SpringExercisesMainConfiguration.class)
                .banner(banner)
                .listeners(listener, closeListener)
                .run(args);
    }
}
