package eu.solidcraft.task5;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

@Configuration
@PropertySource("classpath:optionalDev.properties")
public class OptionalConfiguration {

    @Autowired
    private Environment env;

    @Bean
    @Profile("dev")
    String optionalProperty() {
        return env.getProperty("eu.solidcraft.optionalProperty");
    }

}
