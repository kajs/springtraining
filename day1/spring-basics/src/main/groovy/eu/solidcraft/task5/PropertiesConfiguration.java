package eu.solidcraft.task5;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

@Configuration
@PropertySource("classpath:connection.properties")
public class PropertiesConfiguration {

    @Value("eu.solidcraft.testProperty")
    private String propertyByValue;

    @Bean
    @Autowired
    String propertyFromEnvironment(Environment env) {return env.getProperty("eu.solidcraft.testProperty");}


    @Bean
    @ConfigurationProperties(prefix="connection")
    public ConnectionSettings connectionSettings() {
        return new ConnectionSettings();
    }


}
