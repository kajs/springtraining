package eu.solidcraft.task2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class AnnotatedBookService {

    private AnnotatedBookRepository bookRepository;
    private AnnotatedBookRepository remoteBookRepository;

    @Autowired
    public AnnotatedBookService(
            @Qualifier("inFileBookRepository") AnnotatedBookRepository bookRepository,
            @Qualifier("inMemoryBookRepository") AnnotatedBookRepository remoteBookRepository
    ) {
        this.bookRepository = bookRepository;
        this.remoteBookRepository = remoteBookRepository;
    }

/*
  wstrzykiwanie jedno przez setter drugie przez konstruktor ale to jest ble rozwiazanie
  @Autowired
    AnnotatedBookService(@Qualifier("inMemoryBookRepository") AnnotatedBookRepository remoteBookRepository) {
        this.remoteBookRepository = remoteBookRepository;
    }


    @Autowired(required = false)
    public void setBookRepository(@Qualifier("inFileBookRepository")  AnnotatedBookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }*/
}
