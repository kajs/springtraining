package eu.solidcraft.task2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class AnnotatedTwoConstructorBookService {

    private AnnotatedInMemoryBookRepository inMemoryRepository;
    private AnnotatedInFileBookRepository inFileRepository;

    @Autowired(required = false)
    public AnnotatedTwoConstructorBookService(@Qualifier("inMemoryBookRepository") AnnotatedInMemoryBookRepository inMemoryRepository) {
        this.inMemoryRepository = inMemoryRepository;
    }

    @Autowired(required = false)
    public AnnotatedTwoConstructorBookService(@Qualifier("inMemoryBookRepository") AnnotatedInMemoryBookRepository inMemoryRepository, @Qualifier("inFileBookRepository")AnnotatedInFileBookRepository inFileRepository) {
        this.inMemoryRepository = inMemoryRepository;
        this.inFileRepository = inFileRepository;
    }

    public int numberOfRepositoriesPresent() {
        int numberOfRepos = 0;
        numberOfRepos += inMemoryRepository == null ? 0 : 1;
        numberOfRepos += inFileRepository == null ? 0 : 1;
        return numberOfRepos;
    }

}
