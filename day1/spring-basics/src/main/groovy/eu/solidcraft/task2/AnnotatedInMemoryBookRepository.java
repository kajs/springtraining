package eu.solidcraft.task2;

import org.springframework.stereotype.Repository;

@Repository("inMemoryBookRepository")
public class AnnotatedInMemoryBookRepository implements AnnotatedBookRepository {
}
