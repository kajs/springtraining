package eu.solidcraft;

import eu.solidcraft.task2.config.Task2Configuration;
import eu.solidcraft.task3.JCConfiguration;
import eu.solidcraft.task4.ProfilesConfiguration;
import eu.solidcraft.task5.PropertiesConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({Task2Configuration.class, JCConfiguration.class, ProfilesConfiguration.class, PropertiesConfiguration.class})
public class SpringExercisesMainConfiguration {
}
