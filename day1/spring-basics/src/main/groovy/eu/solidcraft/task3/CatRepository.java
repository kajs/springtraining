package eu.solidcraft.task3;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

public class CatRepository implements PetRepository<Cat> {
    @Override
    public List<Cat> findAll() {
        return newArrayList(new Cat(), new Cat(), new Cat());
    }
}
