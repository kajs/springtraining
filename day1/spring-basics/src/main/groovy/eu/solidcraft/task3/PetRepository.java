package eu.solidcraft.task3;

import java.util.List;

public interface PetRepository<T extends Pet> {
    List<T> findAll();
}
