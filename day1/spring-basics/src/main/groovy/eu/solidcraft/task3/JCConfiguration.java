package eu.solidcraft.task3;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({DogConfig.class, CatConfig.class})
public class JCConfiguration {

}
@Configuration
class DogConfig {

    @Bean(name="dogRepository")
    public DogRepository dogRepository(){
        return new DogRepository();
    }

    @Bean(name="dogService")
    public PetService dogService(){
        return new PetService(dogRepository());
    }
}

@Configuration
class CatConfig {
    @Bean(name="catRepository")
    public CatRepository catRepository(){
        return new CatRepository();
    }

    @Bean(name="catService")
    public PetService catService(){
        return new PetService(catRepository());
    }

    @Bean(name="pussService")
    public PetService pussService(){
        return catService();
    }
}