package info.solidsoft.spring.mvc

import info.solidsoft.spring.SpringMvcApplication
import org.springframework.boot.test.SpringApplicationContextLoader
import org.springframework.boot.test.WebIntegrationTest
import org.springframework.test.context.ContextConfiguration
import spock.lang.Ignore
import spock.lang.Specification

@ContextConfiguration(classes = SpringMvcApplication, loader = SpringApplicationContextLoader)
@WebIntegrationTest("server.port=8090")
class SimpleIntegrationControllerSpec extends Specification {

    private String port = 8090

    @Ignore
    def "should allow to make HTTP request to server"() {
        expect:
            "http://localhost:$port/integration/ping".toURL().getText() == "pong"
    }

    //TODO: How to switch to using a random port? Do required modifications.
}
