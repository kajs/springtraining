package info.solidsoft.spring.mvc

import org.mockito.internal.matchers.NotNull
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MvcResult
import spock.lang.Ignore

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*

class HeaderControllerSpec extends BaseMockMvcSpec {

    private static final String PATH_PREFIX = "/header"

    @Ignore
    def "should get json content"() {
        when:
            MvcResult result = mockMvc.perform(get("$PATH_PREFIX/json"))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andReturn()
        then:
            result.getResponse().getContentAsString() == '{"foo":"bar"}'    //only for simple cases - it's very fragile
    }

    @Ignore
    def "should fail with image request content type"() {
        expect:
            mockMvc.perform(
                    post("$PATH_PREFIX/notImage")
                            .contentType(MediaType.IMAGE_PNG))
                    .andExpect(status().isMethodNotAllowed())
    }

    @Ignore
    def "should have header Correlation-Id set in response"() {
        expect:
            mockMvc.perform(post("$PATH_PREFIX/netCorrelationId"))
                    .andExpect(status().isOk())
                    .andExpect(header().string("Correlation-Id", NotNull.NOT_NULL))
    }

    @Ignore
    def "on post response should have Correlation-Id propagated from request"() {
        expect:
            mockMvc.perform(
                    post("$PATH_PREFIX/propagatedCorrelationId")
                            .header("Correlation-Id", "from-request-cid")
                            .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andExpect(header().string("Correlation-Id", "from-request-cid"))
    }

    @Ignore
    def "should return 400 on 'foobar' in post request"() {
        mockMvc.perform(
                post("$PATH_PREFIX/notWithFoobar")
                        .content("Where is foobar?"))
                .andExpect(status().isBadRequest())

    }
}
