package info.solidsoft.spring.mvc

import org.hamcrest.core.StringContains
import spock.lang.Ignore

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

class ExceptionControllerSpec extends BaseMockMvcSpec {

    private static final String PATH_PREFIX = "/exception"

    @Ignore
    def "should return 503 on NPE"() {
        expect:
            mockMvc.perform(get("$PATH_PREFIX/npe"))
                    .andExpect(status().isInternalServerError())
    }

    @Ignore
    def "should return 501 - Not implemented with message on UnsupportedOperationException"() {
        expect:
            mockMvc.perform(get("$PATH_PREFIX/unsupported"))
                    .andExpect(status().isNotImplemented())
                    .andExpect(content().string(new StringContains("Ups!")))
    }
}
