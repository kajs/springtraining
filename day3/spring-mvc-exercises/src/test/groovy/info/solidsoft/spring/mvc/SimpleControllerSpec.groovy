package info.solidsoft.spring.mvc

import spock.lang.Ignore

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

class SimpleControllerSpec extends BaseMockMvcSpec {

    private static final String PATH_PREFIX = "/simple"

    @Ignore
    def "should get 200 for existing path"() {
        expect:
            mockMvc.perform(get("$PATH_PREFIX/empty"))
                    .andExpect(status().isOk())
    }

    @Ignore
    def "should get 200 with plain text content"() {
        expect:
            mockMvc.perform(get("$PATH_PREFIX/hello"))
                    .andExpect(status().isOk())
                    .andExpect(content().string("Hello MVC"))
    }
}
