package info.solidsoft.spring.mvc

import groovy.json.JsonSlurper
import org.springframework.test.web.servlet.MvcResult
import spock.lang.Ignore

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

class PersonControllerSpec extends BaseMockMvcSpec {

    @Ignore
    def "should get person as JSON"() {
        given:
            def slurperedExpected = new JsonSlurper().parseText('{"firstName":"Klaudyna","lastName":"Dżejsonowa"}')
        when:
            MvcResult result = mockMvc.perform(get("/person/get"))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType("application/json;charset=UTF-8"))
                    .andReturn()
        then:
            def slurperedResponse = new JsonSlurper().parseText(result.getResponse().getContentAsString())
            slurperedResponse == slurperedExpected
    }
}
