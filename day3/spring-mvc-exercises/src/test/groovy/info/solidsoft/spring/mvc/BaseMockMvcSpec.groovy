package info.solidsoft.spring.mvc

import groovy.transform.CompileStatic
import info.solidsoft.spring.SpringMvcApplication
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.SpringApplicationContextLoader
import org.springframework.context.ApplicationContext
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.web.WebAppConfiguration
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.setup.ConfigurableMockMvcBuilder
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.context.WebApplicationContext
import spock.lang.Specification

@CompileStatic
@WebAppConfiguration
@ContextConfiguration(classes = SpringMvcApplication, loader = SpringApplicationContextLoader)
class BaseMockMvcSpec extends Specification {

    @Autowired protected WebApplicationContext webApplicationContext
    @Autowired protected ApplicationContext applicationContext

    protected MockMvc mockMvc

    void setup() {
        ConfigurableMockMvcBuilder mockMvcBuilder = MockMvcBuilders.webAppContextSetup(webApplicationContext)
        mockMvc = mockMvcBuilder.build()
    }
}