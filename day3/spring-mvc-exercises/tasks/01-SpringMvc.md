1. Pojedynczo odingoruj testy w SimpleControllerSpec i stwórz metody w SimpleController, które pozwolą im przejść.  
2. Pojedynczo odingoruj testy w HeaderControllerSpec i stwórz metody w HeaderController, które pozwolą im przejść.  
3. Odingoruj test w PersonControllerSpec i stwórz metodę w PersonController, która pozwoli mu przejść.  
4. Pojedynczo odingoruj testy w ExceptionControllerSpec i stwórz metody w ExceptionrController, które pozwolą im przejść.  
5. Odingoruj test w SimpleIntegrationControllerSpec, stwórz metodę w SimpleIntegrationController oraz zmodyfikuj konfiurację 
klasy testowej, które pozwoli testowi przejść.